// CRUD OPERATIONS
	// heart of any backend application, mastering this is essential for any developer
	// helps in building character and increasing exposure to logical statements the will help us manipulate data.

// [SECTION] Create (Inserting document);
	// since mongoDB deals with object as it's structure for documents, we can easily create them by providing objects into our methods
	// mongoDB shell also uses javascript for its syntax.
	
	// Insert One document
		/*
			Syntax:
				db.collectionName.insertOne({object/document})
		*/

		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "12345",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS","Javascript","Python"],
			department:"none"
		});


	// Insert Many

		/*Syntax
			db.users.insertMany([{objectA}, {objectB}])
		*/

		db.users.insertMany([{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87654321",
				email: "stephen@gmail.com"
			},
			courses:["Python","React","PHP"],
			department:"none"
		}, {
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "87654321",
				email: "neil@gmail.com"
			},
			courses:["React","Laravel","Sass"],
			department:"none"
		}]);

		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "12345",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS","Javascript","Python"]
		});


		//


		db.users.insertOne({
			firstName: "Jane",
			lastName: "Doe",
			age: 21,
			contact:{
				phone: "12345",
				email: "janedoe@gmail.com"
			},
			courses: ["CSS","Javascript","Python"],
			gender: "Female"
		});





// [SECTION] Find (Read);
		// it will show us all documents in our collection

		/*
		Syntax:
			db.collectionName.find();
		*/

		db.users.find()



		// it will show us all documents in our collection that has given fieldset
		/*
		Syntax:
		db.collectionName.find({field:value})
		*/

		db.users.find({age:76})

		db.users.find({firstName:"Jane"});



		// Finding mutiple fieldsets

		db.collectionName.find({fieldA: valueA,fieldB: valueB})

		db.users.find({lastName: "Armstrong", age: 82});







// [SECTION] Updating Documents (Update);
	// Add document
	/*
		Syntax:
			db.collectionName.updateOne({criteria}, {$set:{field:value}});
	*/

//5 Original
	db.users.insertOne({
		firstName: "Test",
		lastName:"Test",
		age: 0,
		contract: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		course: [],
		department:"none"
	})


/*
		Syntax:
			db.collectionName.updateOne({criteria}, {$set:{field:value}});
	*/

// Updated Version

	db.users.updateOne({
		firstName: "Jane"
	},{
			$set: {
				lastName:"Hawking"
			}
	})



db.users.updateMany({firstName: "Jane"},{
	$set:{
		lastName:"Wick",
		age:25
	}
})


db.users.updateMany({department: "none"},{
	$set:{
		department: "HR"
		}
	})




// [SECTION] Replacing Old Documents;

	/*
	Syntax:
	db.users.replaceOne({criteria},{document/objectToReplace})
	*/


	db.users.replaceOne({firstName:"Test"}, {
		firstName: "Chris",
		lastName: "Mortel"
	})





// [SECTION]Deleting Documents;

	//Single
	/*
		Syntax:
			db.collectionName.deleteOne({criteria});
	*/
	

	db.users.deleteOne({firstName: "Jane"});


	// Delete Many
	/*
		Syntax:
			db.collectionName.deleteMany({criteria});
	*/

	db.users.deleteMany({firstName: "Jane"});


	// reminder
		// db.collectionName.deleteMany()
		// Remind: dont forget to add criteria



// [SECTION] Advanced Queries;
		// embedded - value ng property naka - object
		// nested field - array


	// query on an embedded
	// use dot .notation and make it a "string"

	db.users.find({"contact.phone": "87654321"});

	// query an array with exact elements
	db.users.find({courses:["React", "Laravel","Sass"]})


	// querying an array regardless of order and elements

	db.user.find({courses: {$all: ["React"]}});